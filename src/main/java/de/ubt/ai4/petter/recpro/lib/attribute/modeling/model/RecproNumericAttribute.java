package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RecproNumericAttribute extends RecproAttribute{
    private double defaultValue;
    private RecproAttributeType attributeType = RecproAttributeType.NUMERIC;
}
