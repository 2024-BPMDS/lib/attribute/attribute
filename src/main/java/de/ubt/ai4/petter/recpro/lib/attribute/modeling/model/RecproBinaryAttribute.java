package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RecproBinaryAttribute extends RecproAttribute {
    private boolean defaultValue;
    private RecproAttributeType attributeType = RecproAttributeType.BINARY;
}
