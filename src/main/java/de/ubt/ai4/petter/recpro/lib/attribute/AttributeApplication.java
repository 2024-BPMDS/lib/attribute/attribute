package de.ubt.ai4.petter.recpro.lib.attribute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttributeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttributeApplication.class, args);
	}

}
