package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

public enum RecproAttributeType {
    TEXT, NUMERIC, OBJECT, BINARY, META;
    public static RecproAttributeType findType(String type) {
        return switch (type) {
            case "NUMERIC" -> NUMERIC;
            case "OBJECT" -> OBJECT;
            case "BINARY" -> BINARY;
            case "META" -> META;
            default -> TEXT;
        };
    }

}
